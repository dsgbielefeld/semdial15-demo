package app;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import module.DialogueManager_Pentoland;
import module.TimeoutPentoland;
import sium.module.ResolutionModuleDisc;
import sium.nlu.context.Context;
import sium.nlu.language.LingEvidence;
import sium.system.util.TakeCVSqlUtils;
import wac.util.TakeRawSqlUtils;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.source.IUDocument;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import inpro.io.rsb.RsbListenerModule;

public class Interactive {
	
//	@S4Component(type = WebSpeech.class)
//	public final static String PROP_CURRENT_HYPOTHESIS = "webSpeech";
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_CURRENT_HYPOTHESIS = "currentASRHypothesis";	
	
	@S4Component(type = TextBasedFloorTracker.Listener.class)
	public final static String PROP_FLOOR_MANAGER_LISTENERS = TextBasedFloorTracker.PROP_STATE_LISTENERS;
	
	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";	
	
	private static ConfigurationManager cm;
	private static IUDocument iuDocument;
	private static PropertySheet ps;
	private List<PushBuffer> hypListeners;
	
	static List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	static int currentFrame = 0;
	static JTextField textField;
	
	private TakeCVSqlUtils take;
	
	public static double decisionPoint = 0.25;
	
	
	public static void main(String[] args) {
		try {
			new Interactive().run();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}

	private void run() throws SQLException, IOException, UnsupportedAudioFileException {
		try {
			
			cm = new ConfigurationManager(new File("src/config/config.xml").toURI().toURL());
			ResolutionModuleDisc resolution =  (ResolutionModuleDisc) cm.lookup("disc");
			
			System.out.print("Training WAC...");
			resolution.toggleTrainMode();
			take = new TakeCVSqlUtils();
			take.createConnection();
			ArrayList<String> episodes = take.getAllEpisodes();
			
//			setup the training data
			for (String episode : episodes) {
//				System.out.println(episode);
				setContext(resolution, episode);
				
				ArrayList<LingEvidence> ling = take.getLingEvidence(episode, "t");
				setGoldSlots(resolution, episode);
				processLine(resolution, ling);
			}
			
			System.out.println(" WAC training done.");
			
			
			
			ps = cm.getPropertySheet(PROP_CURRENT_HYPOTHESIS);
//			WebSpeech webSpeech = (WebSpeech) cm.lookup(PROP_CURRENT_HYPOTHESIS);
			
			
//			DialogueManager_Pentoland dm =  (DialogueManager_Pentoland) cm.lookup("dm_p");
//			dm.updateState("State", "start");   
			
			RsbListenerModule rsb = (RsbListenerModule) cm.lookup("rsblistener");
			rsb.iulisteners = cm.getPropertySheet("rsblistener").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
			
//			RsbListenerModule rsbReset = (RsbListenerModule) cm.lookup("rsblistener_reset");
//			rsbReset.iulisteners = cm.getPropertySheet("rsblistener_reset").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
			

			
			resolution.train();
			resolution.toggleNormalMode();
			
			
//			TimeoutPentoland tm = (TimeoutPentoland) cm.lookup("timeout");
//			tm.iulisteners = cm.getPropertySheet("timeout").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
			
			hypListeners = ps.getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
			TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
			iuDocument = new IUDocument();
			iuDocument.setListeners(hypListeners);
			SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
			setContext(resolution, "r9.92.p1");
			
			DialogueManager_Pentoland dm =  (DialogueManager_Pentoland) cm.lookup("dm_p");
			dm.updateState("State", "start");
			
				
			Thread startSimpleReco = new Thread(new Runnable() {
			    public void run() {
			    	
//			    	-M -K -Kport 5010 -Kserver localhost
					String[] args = new String[6];
					args[0] = "-M";
					args[1] = "-K";
					args[2] = "-Kport";
					args[3] = "5010";
					args[4] = "-Kserver";
					args[5] = "localhost";
			    	RecoCommandLineParser clp = new RecoCommandLineParser(args);
			    	SimpleReco simpleReco = null;
			    	
			    	while (true) { 
			    	
						try {					
							
						    simpleReco = new SimpleReco(cm, clp);
							
					    	simpleReco.recognizeInfinitely();
					    	
	
						} 
						catch (Exception e) {
							e.printStackTrace();
						} 
						finally {
							if (simpleReco != null) {
								simpleReco.getRecognizer().deallocate();
								simpleReco.shutdownMic();
							}
						}
							
						try {
							System.out.println("Attemping to reconnect to Kaldi server...");
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
			    	}
			    }
			});
			
//			comment out this line if you want to turn off the Kaldi server
			startSimpleReco.start();

			

//			DialogueManager_Pentoland dm =  (DialogueManager_Pentoland) cm.lookup("dm_p");
//			dm.updateState("State", "start");

		} 
		catch (PropertyException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
//		catch (UnsupportedAudioFileException e) {
//			e.printStackTrace();
//		}
		
		turnOffLoggers();
		
	}
	
	
	
	private void turnOffLoggers() {
		List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
		loggers.add(LogManager.getRootLogger());
		for ( Logger logger : loggers ) {
		    logger.setLevel(Level.OFF);
		}		
	}

	static public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}
	
	private void setGoldSlots(ResolutionModuleDisc resolution, String episode) throws SQLException {
		String gold = take.getGoldPiece(episode);
		resolution.setReferent(gold);
	}
	
	private void processLine(ResolutionModuleDisc resolution, ArrayList<LingEvidence> ling) {
		
		ArrayList<WordIU> ius = new ArrayList<WordIU>();
		
		WordIU prev = WordIU.FIRST_WORD_IU;
		
		for (LingEvidence ev : ling) {
			String word = ev.getValue("w1");
			if (word.equals("<s>")) continue;
			WordIU wiu = new WordIU(word, prev, null);
			resolution.addTrainingInstance(wiu);
//			ius.add(wiu);
//			edits.add(new EditMessage<IU>(EditType.ADD, wiu));
//			notifyListeners(hypListeners);
			prev = wiu;
		}
		
		for (WordIU iu : ius) {
			edits.add(new EditMessage<IU>(EditType.COMMIT, iu));
		}
		notifyListeners(hypListeners);
	}	
	
	private void setContext(ResolutionModuleDisc resolution, String episode) throws SQLException {
		Context<String,String> context = take.getRawFeatures(episode);
		resolution.setContext(context);
	}	

}
