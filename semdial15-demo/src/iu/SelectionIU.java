package iu;

import inpro.incremental.unit.IU;

public class SelectionIU extends IU {

	private String id;
	
	public SelectionIU(String id) {		
		setId(id);
	}
	
	@Override
	public String toPayLoad() {
		return getId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
