package module;



import java.util.Collection;
import java.util.List;

import module.opendial.DialogueDecision;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4String;
import opendial.DialogueSystem;
import opendial.datastructs.Assignment;
import opendial.domains.Domain;
import opendial.readers.XMLDomainReader;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import inpro.io.SensorIU;


public class DialogueManager extends IUModule {
	
	@S4String(defaultValue = "")
	public final static String DOMAIN_PROP = "domainFile";		
	
	@S4Component(type = DialogueDecision.class)
	public final static String SSP_PROP = "decision";
	
	DialogueSystem system;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up DialogueManager");
		
		Domain domain = XMLDomainReader.extractDomain(ps.getString(DOMAIN_PROP));
		system = new DialogueSystem(domain);
		system.startSystem();
		
		system.attachModule((DialogueDecision)ps.getComponent(SSP_PROP));
//			system.detachModule(ForwardPlanner.class);
//			StatePruner.ENABLE_PRUNING = false;
		system.getSettings().showGUI = false;
		system.startSystem(); 
	}


	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
	
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			System.out.println("DM GOT SOMETHING: " + edit);
			if (iu instanceof WordIU) {
				updateState("Speech", iu.toPayLoad());
			}
			else if (iu instanceof SensorIU) {
				updateState("HeadNod", iu.toPayLoad());
			}
		}
		
	}
	
	private void updateState(String attribute, String value) {

		
		system.addContent(new Assignment(attribute, value));
			
//			Add partially-observed variables
//			CategoricalTable t = new CategoricalTable();
//		 	t.addRow(new Assignment("a_u", "DoA"), 0.7);
//		 	t.addRow(new Assignment("a_u", "DoC"), 0.2);
//		 	t.addRow(new Assignment("a_u", "DoB"), 0.1);
//			system.addContent(t);
			
		
	 	
	}

}
