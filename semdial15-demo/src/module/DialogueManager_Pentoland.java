package module;

import java.util.Collection;
import java.util.List;
import java.util.Date;

import module.opendial.DialogueDecision;
import module.opendial.SelectionDecision;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4String;
import opendial.DialogueSystem;
import opendial.datastructs.Assignment;
import opendial.domains.Domain;
import opendial.gui.GUIFrame;
import opendial.readers.XMLDomainReader;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import iu.SelectionIU;


public class DialogueManager_Pentoland extends IUModule {
	
	@S4String(defaultValue = "")
	public final static String DOMAIN_PROP = "domainFile";		
	
	@S4Component(type = DialogueDecision.class)
	public final static String SSP_PROP = "Sys_Speech";
	
	@S4Component(type = SelectionDecision.class)
	public final static String SEL_PROP = "Selection";
	
	DialogueSystem system;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up DialogueManager");
		
		Domain domain = XMLDomainReader.extractDomain(ps.getString(DOMAIN_PROP));
		system = new DialogueSystem(domain);
		system.startSystem();
		system.attachModule((DialogueDecision)ps.getComponent(SSP_PROP));
		system.attachModule((SelectionDecision)ps.getComponent(SEL_PROP));
//			system.detachModule(ForwardPlanner.class);
//			StatePruner.ENABLE_PRUNING = false;
		system.getSettings().showGUI = false;
		system.startSystem();
		                                                       		
		
	}


	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			
			if (iu instanceof TimeoutIU) {
				System.out.println("DM got TimeoutIU: " + iu);
	     		updateState("Timeout_Signal", iu.toPayLoad());
				continue;
			}
			
			
			if ((iu instanceof SelectionIU) && (iu.toPayLoad() != "-1")) {             //WACrr has selected a piece
				System.out.println("DM got SelectionIU: ");
	     		updateState("User_Speech", "performing");
	     		updateState("State", "wait_for_user");
	     		continue;
		    }
			
			
			if (iu instanceof WordIU) {
				switch (edit.getType()) {
				case ADD:
					updateState("User_Speech", iu.toPayLoad());
					break;
				case COMMIT:
					break;
				case REVOKE:
					break;
				default:
					break;							
			}
				
			}
//			else if (iu instanceof SensorIU) {
//				updateState("HeadNod", iu.toPayLoad());
//			}
		}
	}
	
	public void updateState(String attribute, String value) {
		
//		    Date date_dm = new Date();
//		    for (int i=0; i<10; i++) {System.out.println("DM received " + value  + " from " + attribute + " at " + date_dm.getTime());}
//		    System.out.println("DM received " + value  + " from " + attribute + " at " + date_dm.getTime());
			system.addContent(new Assignment(attribute, value));
			
			
			
//			Add partially-observed variables
//			CategoricalTable t = new CategoricalTable();
//		 	t.addRow(new Assignment("a_u", "DoA"), 0.7);
//		 	t.addRow(new Assignment("a_u", "DoC"), 0.2);
//		 	t.addRow(new Assignment("a_u", "DoB"), 0.1);
//			system.addContent(t);
			

		
	 	
	}

}
