package module;

import inpro.apps.SimpleMonitor;
import inpro.audio.DispatchStream;
import inpro.incremental.IUModule;
import inpro.incremental.processor.AdaptableSynthesisModule;
import inpro.incremental.sink.LabelWriter;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SysInstallmentIU;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;

public class Generation extends IUModule {
	
	@S4Component(type = LabelWriter.class)
	public final static String PROP_LABEL_WRITER = "labelWriter";
	
	int stimulusID;
	ArrayList<Integer> randomStimulusIDs;
	String knowledgeFile;
	DispatchStream speechDispatcher;
	private AdaptableSynthesisModule synthesisModule;
	Process pr;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up Generation");
		// normal program flow
	    speechDispatcher = SimpleMonitor.setupDispatcher();
		synthesisModule = new AdaptableSynthesisModule(speechDispatcher);
	}

	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) { 
		
		for (EditMessage<? extends IU> edit : edits) {
<<<<<<< HEAD
			String audiofile = "resources/Prompts/" + edit.getIU().toPayLoad() + ".wav";
//			StdAudio.play(audiofile);
			try {
				InputStream in = new FileInputStream(audiofile);
				// create an audiostream from the inputstream
			    AudioStream audioStream = new AudioStream(in);
			    // play the audio clip with the audioplayer class
			    AudioPlayer.player.start(audioStream);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		    
=======

			if(!edit.getIU().toPayLoad().equals("None")) {

				String audiofile = "resources/Prompts/" + edit.getIU().toPayLoad() + ".wav";
				System.out.println("PLAYING " + edit.getIU().toPayLoad());
				 try {
					 
//		            play the audio clip with the audioplayer class
//					AudioPlayer.player.start(new AudioStream(new FileInputStream(audiofile)));
					if (pr != null) {
						while (pr.isAlive()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
					 
					pr  = Runtime.getRuntime().exec("mplayer " + audiofile);
		            } catch (FileNotFoundException e) {
		                e.printStackTrace();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
			}
>>>>>>> 07c7f8817d7019d4bacb16af6a80ac7c21325542
		}
			
	}

	

	private void play(final String text2) {
		new Thread() {
			public void run() {
				String text = text2;
				text = text.replaceAll(":", ""); // get rid of colons, which mess up pitch optimization
				text = text.replaceAll(" \\| ", " ");
				SysInstallmentIU nonincremental = new SysInstallmentIU(text);
				speechDispatcher.playStream(nonincremental.getAudio(), true);
//			    Date time = new Date();
//				System.out.println("playing " + text + " at " + time.getTime());
				speechDispatcher.waitUntilDone();
			}
		}.start();
	}
	

	

}
 
