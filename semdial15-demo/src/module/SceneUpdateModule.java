package module;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import sium.module.ResolutionModuleNgram;
import sium.nlu.context.Context;
import util.XmlContextUtil;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;

public class SceneUpdateModule extends IUModule {
	
	@S4Component(type = ResolutionModuleNgram.class)
	public final static String RESOLVER = "resolver";
	
	@S4Integer(defaultValue = -1)
	public final static String TIMEOUT = "timeout";
	
	private Context<String,String> scene;
	private ResolutionModuleNgram resolution;
	private int timeout;
	private Thread timeoutThread;
	
	@Override
	public void newProperties(PropertySheet ps) {
		super.newProperties(ps);
		scene = new Context<String,String>();
		resolution = (ResolutionModuleNgram) ps.getComponent(RESOLVER);
		timeout = ps.getInt(TIMEOUT);
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		
		
		for (EditMessage<? extends IU> edit : edits) {
			switch (edit.getType()) {
			case ADD:
				try {
					if (edit.getIU().toPayLoad().equals("<reset>")) {
						resolution.reset();
						continue;
					}
					XmlContextUtil.updateContext(edit.getIU().toPayLoad(), scene);
					resolution.setContext(scene);
					
					checkTimeout();
					
					
				} 
				catch (ParserConfigurationException e) {
					e.printStackTrace();
				} 
				catch (SAXException e) {
					e.printStackTrace();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case COMMIT:
				break;
			case REVOKE:
				break;
			default:
				break;
			
			}
			
		}
		

	}

	private void checkTimeout() {
		
		if (timeoutThread != null) 
			timeoutThread.interrupt();
//		create a timeout ... if nothing happens after <timeout> seconds, then reset the context
		if (timeout != -1) {
			timeoutThread = new Thread(new Runnable() {
			    public void run() {
			        try {
						Thread.sleep(timeout * 1000);
					} 
			        catch (InterruptedException e) {
//			        	if something happened within the timeout, then just return (i.e., don't reset the context)
			        	return;
					}
			        scene = new Context<String,String>();
			    }
			});
			timeoutThread.start();
		}		
	}

}
