package module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import sium.iu.SlotIU;
import sium.nlu.stat.Distribution;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import iu.SelectionIU;

public class SlotDecision extends IUModule {
	
	private double threshold = 0.05;

	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
	}
	
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		
		List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			

			
			if (iu instanceof SlotIU) {
				switch (edit.getType()) {
				case ADD:
					SlotIU siu = (SlotIU) iu;
					Distribution<String> dist = siu.getDistribution();
					if (dist.isEmpty()) continue;
					double margin = dist.getMargins().get(0);
					if (margin > threshold) {
						SelectionIU selectIU = new SelectionIU(dist.getArgMax().getEntity());
						newEdits.add(new EditMessage<SelectionIU>(EditType.ADD, selectIU));
					}
					break;
				case COMMIT:
					break;
				case REVOKE:
					break;
				default:
					break;
				
				}
				
			}
		}
//		if nothing is selected, always reset whether or not ADD or REVOKE
//		if (!selectedSent) {
//			SelectionIU selectIU = new SelectionIU("-1");
//			newEdits.add(new EditMessage<SelectionIU>(EditType.ADD, selectIU));
//		}
		
		rightBuffer.setBuffer(newEdits);
		
	}

}
