package module;

import java.util.List;

import edu.cmu.sphinx.linguist.dictionary.Pronunciation;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

public class TimeoutIU extends WordIU {

	public TimeoutIU(WordIU sll) {
		super(sll);
	}

	public TimeoutIU(String spelling, WordIU sll, List<IU> groundedIn) {
		super(spelling, sll, groundedIn);
	}


}