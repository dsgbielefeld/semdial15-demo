package module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import opendial.datastructs.Assignment;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;
import inpro.incremental.IUModule;
import inpro.incremental.PushBuffer;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SysInstallmentIU;
import inpro.incremental.unit.WordIU;

public class TimeoutPentoland extends IUModule {
	
//	@S4Integer(defaultValue = 7000)
//	public final static String TIME_PROP = "timeout";		
	
	int timeout;
	Thread timeoutThread;
	private List<PushBuffer> hypListeners;
	
	private int minWait;					       		//minimum timeout duration

	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
//		timeout = ps.getInt(TIME_PROP);
		minWait = 8000;
		startNewThread();                                //To get it to run when the system starts, before user speech
	}
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
//		minWait = 3000;
		startNewThread();
	}
	
	private void startNewThread() {

		final int pressure_levels = 2;

//		final int wait = minWait;
		
	    	if (timeoutThread != null && timeoutThread.isAlive()) {          //lo de not null es para que funcione la primera vez
				
				timeoutThread.interrupt();
			}
			
			
			timeoutThread = new Thread(new Runnable() {
				public void run() {

					int pressure_counter = 2;                           
				    int pressure_level;
					
				   
				    while(true) {
				    	
						try {
							
							double random = Math.random();
							timeout = (int) (random * 3000) + minWait ;    // min timeout 2,5 seconds, max timeout 3,5 seconds (except for the very first iteration)
						    Date date = new Date();
//							for (int i=0; i<10; i++) {System.out.printf("Timeout duration: %d ms generated at %d \n", timeout, date.getTime());}
						    
						    System.out.printf("Timeout duration: %d ms generated at %d \n", timeout, date.getTime());
						    Date date2 = new Date();
							Thread.sleep(timeout);
							
//							for (int i=0; i<10; i++) {System.out.printf("Timeout ended at: %d \n", date2.getTime());}
							System.out.printf("Timeout ended at: %d \n", date2.getTime());
							
	            			pressure_level = pressure_counter % pressure_levels;
							String pressure_level_string = Integer.toString(pressure_level);
                            		
							ArrayList<EditMessage<TimeoutIU>> newEdits = new ArrayList<EditMessage<TimeoutIU>>();
							newEdits.add(new EditMessage<TimeoutIU>(EditType.ADD, new TimeoutIU(pressure_level_string, null, null)));
	            			rightBuffer.setBuffer(newEdits);
	            			rightBuffer.notify(hypListeners);
	            			
	            			++ pressure_counter;
	            			
	            			minWait = 2500;
	            			
						}
							
					    catch (InterruptedException e) {
//							e.printStackTrace();
					    	break;
						}	

					}
				  }
				});

	
		timeoutThread.start();
		
	}
	

}
