package module.opendial;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import opendial.DialogueState;
import opendial.bn.distribs.CategoricalTable;
import opendial.bn.values.Value;
import opendial.datastructs.Assignment;
import opendial.modules.Module;

public class DialogueDecision extends IUModule implements Module  {

	boolean paused = false;
	
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up DialogueDecision");
	}
	
	@Override
	public void start() {

		paused = false;
	}

	@Override
	public void trigger(DialogueState state, Collection<String> updatedVars) {
		
		System.out.println("got state update " +state.queryProb("User_Speech").getBest().toString());
		System.out.println("selection state " +state.queryProb("Selection").getBest().toString());
		//TODO: this should only be called when certain decisions are made
		ArrayList<EditMessage<WordIU>> newEdits = new ArrayList<EditMessage<WordIU>>();
		
		if (updatedVars.contains("Sys_Speech")) {
			CategoricalTable utt = state.queryProb("Sys_Speech").toDiscrete();
			Value randomUtt = utt.sample(); 
		
			String prompt = randomUtt.toString();
			
//			for (Value value : utt.getValues()) {}
//			System.out.println(state.queryProb("Sys_Speech").toDiscrete().getValues());

//			System.out.println("SYSTEM UTT: " + prompt);
			newEdits.add(new EditMessage<WordIU>(EditType.ADD, new WordIU(prompt, null, null)));
		}
		
		
//		if (updatedVars.contains("Sys_Speech_2")) {
//			CategoricalTable utt = state.queryProb("Sys_Speech_2").toDiscrete();
//			Value randomUtt = utt.sample(); 
//		
//			String prompt = randomUtt.toString();		
//			newEdits.add(new EditMessage<WordIU>(EditType.ADD, new WordIU(prompt, null, null)));
//		}
		
//		state.addToState(new Assignment("User_Speech", "<fake>"));
//		state.setAsNew();

		if (!newEdits.isEmpty()) {
			rightBuffer.setBuffer(newEdits);
			super.notifyListeners();
		}
	}

	/**
	 * Pauses the module.
	 */
	@Override
	public void pause(boolean toPause) {
		paused = toPause;
	}

	/**
	 * Returns true is the module is not paused, and false otherwise.
	 */
	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// This doesn't do anything, we only need a right buffer in this class
		
	}

}
