package module.opendial;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import iu.SelectionIU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import opendial.DialogueState;
import opendial.bn.distribs.CategoricalTable;
import opendial.bn.values.Value;
import opendial.datastructs.Assignment;
import opendial.modules.Module;
import sium.module.ResolutionModuleDisc;

public class SelectionDecision extends IUModule implements Module  {

	boolean paused = false;
	
	@S4Component(type = ResolutionModuleDisc.class)
	public final static String WAC_PROP = "WAC";
	
	
	ResolutionModuleDisc wac;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up DialogueDecision");
		wac = (ResolutionModuleDisc)ps.getComponent(WAC_PROP);
	}
	
	@Override
	public void start() {

		paused = false;
	}

	@Override
	public void trigger(DialogueState state, Collection<String> updatedVars) {
		//TODO: this should only be called when certain decisions are made
		ArrayList<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		
		if (updatedVars.contains("Selection")) {
			Value val = state.queryProb("Selection").toDiscrete().getBest();
			
//			turn off WAC
			if ("confirm_pending".equals(val.toString())) {
				wac.setIgnore();
			}
			
//			turn WAC back on and remove the CV outline
			if ("accepted".equals(val.toString()) || "rejected".equals(val.toString())) {
				wac.reset();
				wac.setListen();
				SelectionIU selectIU = new SelectionIU("-1");
				newEdits.add(new EditMessage<SelectionIU>(EditType.ADD, selectIU));
				
				state.addEvidence(new Assignment("Selection", "waiting"));
			}
//			for (Value value : utt.getValues()) {}
//			System.out.println(state.queryProb("Sys_Speech").toDiscrete().getValues());

//			System.out.println("SYSTEM UTT: " + prompt);
		}
//		state.addToState(new Assignment("User_Speech", "<fake>"));
//		state.setAsNew();
		
		


		if (!newEdits.isEmpty()) {
			rightBuffer.setBuffer(newEdits);
			super.notifyListeners();
		}
	}

	/**
	 * Pauses the module.
	 */
	@Override
	public void pause(boolean toPause) {
		paused = toPause;
	}

	/**
	 * Returns true is the module is not paused, and false otherwise.
	 */
	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// This doesn't do anything, we only need a right buffer in this class
		
	}

}
