package util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sium.nlu.context.Context;

public class XmlContextUtil {

	public static void updateContext(String xmlString, Context<String,String> scene) throws ParserConfigurationException, SAXException, IOException {
		
		synchronized (scene) { 
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    Document doc = dBuilder.parse(new InputSource(new StringReader(xmlString)));
	    
	    doc.getDocumentElement().normalize();
	    
	    String id = doc.getDocumentElement().getAttribute("id");
	    
	    scene.removeEntityAndProperties(id);
	    
	    String x = getValue(doc, "position", "x");
	    String y = getValue(doc, "position", "y");
	    String nEdges = getValue(doc, "nbEdges", "value");
	    
	    String h = getValue(doc, "hsvValue", "H");
	    String s = getValue(doc, "hsvValue", "S");
	    String v = getValue(doc, "hsvValue", "V");
	    
	    String r = getValue(doc, "rgbValue", "R");
	    String g = getValue(doc, "rgbValue", "G");
	    String b = getValue(doc, "rgbValue", "B");
	    
	    scene.addPropertyToEntity(id, prop("x", x));
	    scene.addPropertyToEntity(id, prop("y", y));
	    scene.addPropertyToEntity(id, prop("ne", nEdges));
	    scene.addPropertyToEntity(id, prop("h", h));
	    scene.addPropertyToEntity(id, prop("s", s));
	    scene.addPropertyToEntity(id, prop("v", v));
	    scene.addPropertyToEntity(id, prop("r", r));
	    scene.addPropertyToEntity(id, prop("g", g));
	    scene.addPropertyToEntity(id, prop("b", b));
		}
	    
	}
	
	
	private static String prop(String string, String x) {
		return string + ":" + x;
	}


	private static String getValue(Document doc, String type, String key) {
		NodeList list = doc.getElementsByTagName(type);
	    return list.item(0).getAttributes().getNamedItem(key).getNodeValue();
	}
	
}
