
Steps for starting the demo:
----------------------------


1) Renaming the camera so that the external one is used instead of the in-built one:

   sudo mv /dev/video1 /dev/video0


2) Starting Kaldi:

cd /home/dsg-labuser/semdial15/kaldi/egs/dsg-bielefeld/s5/exp/tri3b_fmmi_d
/home/dsg-labuser/semdial15/kaldi/src/onlinebin/online-audio-server-decode-faster --verbose=1 --rt-min=0.5 --rt-max=1.0 --max-active=6000 --beam=72.0 --acoustic-scale=0.0769 final.mdl ../tri3b/graph/HCLG.fst ../tri3b/graph/words.txt '1:2:3:4:5' ../tri3b/graph/phones/word_boundary.int 5010 final.mat


3) Starting all the InproTK/opendial modules:

   a) open Eclipse
   b) run Interactive


4) Starting the CV module:

   This can be done: - from Eclipse, by running cvModule.py 
                       or
                     - from the terminal: cd /home/dsg-labuser/semdial15/010-open_cv/code/cvModule/src
python cvModule.py -video 0 -rsb  


-----------------------------------------
OTHER NOTES:

- For camera settings (with the Logitech camera we're using now), you can do a Ubuntu search for GUVCVIdeo

- It might be a good idea to check the system monitor from time to time, since Kaldi and OpenCV eat up a lot of resources. If CPU activity is too high, you may wanna restart everything.

------------------------------------------

Have fun playing!!

